console.log("Variable vs Array");

/*list of studentIDs of all graduating student of the class*/
let studentNumberA = "2020-1923"
let studentNumberB = "2020-1924"
let studentNumberC = "2020-1925"
let studentNumberD = "2020-1926"
let studentNumberE = "2020-1927"

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);


/*                          0            1            2             3*/
let studentNumbers = [ "2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"]; /*inside the square brackets what we called elements*/
console.log(studentNumbers);

console.log("=================================================")
/*SECTION - arrays*/

/*
        - Arrays are used to store multiple related values in a single variable.
        - They are declared using square brackets ([]) also known as "Array Literals"
        - Arrays it also provides access to a number of functions/methods that help in manipulation array.
            - Methods are used to manipulate information stored within the same object.
        - Array are also objects which is another data type.
        - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
        - Syntax:
            let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
    */

console.log("Examples of array: ");

/*common examples of arrays*/
let grades = [98.5, 94.3, 89.2, 90]; /*arrays could store numeric calues*/
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Gateway", "Toshiba", "Fujitso"];
/*arrays could store string values*/

console.log(grades);
console.log(computerBrands);

/*possible use on an array but it is not recommended*/
/*it is not recommended for it is hard to define what is the purpose or the data inside the array and where the programmer of co-programmer*/
let mixedArr = ["John", "Doe", 12, false, null, undefined, {}];
console.log(mixedArr);

/*alternative way of creating an array*/

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake mongoDb"
]
console.log(myTasks);

console.log("====================================================");

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

console.log("===================================================");

/*section .length property*/
/*".length" property allows us to get and set the total number of items/elements*/

console.log("Using .length property for array size: ");
console.log("Length / size of myTasks array: " +myTasks.length);/*4 elements*/
console.log("Length / size of cities array: " + cities.length); /*3 elements*/

console.log("============================================");

console.log("Using .length property for string size: ");

let fullName = "Leonardo A. Malachico";
console.log("Length/size of fullName: "+ fullName.length);

console.log("=======================================");


/*let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake mongoDb"
]*/
console.log("Removing the last element from an array");
myTasks.length = myTasks.length - 1; /*bake mongoDb was remove since it was minus 1*/

console.log(myTasks.length);
console.log(myTasks);

/*to delete a specific item an array we can employ array methods. we have shown algorithm of "pop method*/;

/*cities = [city1, city2, city3];*/
cities.length--; /*minus 2*/
console.log(cities);

/*fullName = "Leonardo A. Malachico";*//*we cant do the same on string only in array*/
fullName.length = fullName.length -1;
console.log(fullName.length);
console.log(fullName);

console.log("===========================================")

/*we can also add the length of an array.*/
console.log("Add an element to an array");

				/*   0       1        2        3*/
let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);

theBeatles[theBeatles.length] = "Cardo";
console.log(theBeatles);

console.log("===========================================")

// [SECTION] Reading from Arrays

    /*
        - Accessing array elements is one of the common task that we do with an array.
        - This can be done through the use of array indexes.
        - Each element in an array is associated with it's own index number.
        - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
        - Array indexes it is actually refer to a memory address/location

        Array Address: 0x7ffe942bad0
        Array[0] = 0x7ffe942bad0
        Array[1] = 0x7ffe942bad4
        Array[2] = 0x7ffe942bad8

        - Syntax
            arrayName[index];
    */

/*grades = [98.5, 94.3, 89.2, 90];*/
console.log(grades[0]); /*index number of the 1st element is zero*/

/*                   0       1        2       3        4          5
computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Gateway", "Toshiba", "Fujitso"];*/
console.log(computerBrands[3]);

/*acessing an array element that does not exist will return undefined*/
console.log(grades[20]);  /*undefined*/

                   
let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem", "West"];
/*display shaq and magic*/
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

console.log("===============================");

console.log("Reassigning an element from an array");

/*you can also reassign array values using indeces*/

	console.log("Array before reassignment: ");
	console.log(lakersLegends);

	lakersLegends[2] = "Gasol";
	console.log("Array after reassignment: ");
	console.log(lakersLegends);

console.log("================================");

console.log("Access the last element of an array");
/*                      0         1         2        3        4*/
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

console.log("======================================");

console.log("Adding new items into an array using indeces");
/*adding items into an array
we can add items in array using indeces*/ 

const newArr = [];
console.log(newArr[0]); /*undefined*/

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);  /*undefined*/
newArr[1] = "Tifa Lockhart";
console.log(newArr);

console.log("=========================================");

console.log("add element in the hand of an array using '.length' property");

/*Cloud strife   Tife Lockhart Barret Wallace*/
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

console.log("=====================================");

console.log("Displaying the content of an array 1 by 1 through a loop");

/*example of accessing array elements 1 by 1 with console.log*/
/*console.log(newArr[0]);*/ /*use this if only one element*/

/*Cloud strife   Tife Lockhart Barret Wallace*/
/*initialize/declared let i=0*/
/*condition i < newArr.length*/
/*change of value i++*/
for (let index=0; index < newArr.length; index++){
	/*to be able to show each array items instead using console.log*/
		console.log(newArr[index]);
}

console.log("============================");

console.log("Filtering an array using loop and conditional statements: ");
let numArr2 = [5, 12, 30, 46, 40, 52];

for (let index=0; index < numArr2.length; index++){
	if(numArr2[index] % 5 ==0){
		console.log(numArr2[index] + "is divisible by 5.");
	}
	else{
		console.log(numArr2[index] + "is not divisible by 5.")
	}
}


/*section - mutildimensional arrays*/

// [SECTION] Multidimensional Arrays
/*
    -Multidimensional arrays are useful for storing complex data structures.
    - A practical application of this is to help visualize/create real world objects.
    - This is frequently used to store data for mathematic computations, image processing, and record management.
    - Array within an Array
*/

/*create chessboard*/

let chessBoard = [
    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.table(chessBoard);

/*access an element of a multidimensional arrays*/
/*syntax: multiArr[outerArr][innerArr]*/

console.log(chessBoard[3][4]); /*e4*/

console.log("Pawn moves " + chessBoard[2][5])

// arr = []
